#include "Z2n1.h"
#include "common.h"
#include <bits/stdc++.h>

using namespace std;

bool check_cond(const Subset &R, const Subset &Y, const Subset &B) {
  int n = R.n;
  assert(Y.n == n && B.n == n);
  for (int x = -n; x <= n; x++) {
    if (R.contains(x) != R.contains(-x)) {
      return false;
    }
  }
  Subset r_y = R + Y;
  Subset x_b = Subset::X(n);
  x_b.setdiff_inplace(B);
  if (r_y != x_b) {
    return false;
  }
  Subset Z = Subset::full(n);
  if (R + R != Z || Y + Y != Z || B + B != Z) {
    return false;
  }
  return true;
}

struct Res {
  Subset r, y, b;
  friend ostream &operator<<(ostream &os, const Res &r) {
    os << "r = " << r.r << ";\n";
    os << "y = " << r.y << ";\n";
    os << "b = " << r.b << ";\n";
    return os;
  }
};

/// ValuationFn: int (const Subset &r, const Subset &y, const Subset &b)
/// CandContainer: Iterator of int
template <typename ValuationFn, typename CandContainer>
optional<int> try_maximize(Subset &r, Subset &y, Subset &b, ValuationFn vfn,
                           Subset *insert_into, CandContainer candidates) {
  if (candidates.size() == 0) {
    return optional<int>();
  }
  int current_max_score, current_max_cand;
  bool has_current_max = false;
  for (int cand : candidates) {
    if (cand < 0) {
      continue;
    }
    Subset &target = *insert_into;
    bool existed = target.contains(cand);
    int res_score;
    if (existed) {
      res_score = vfn(r, y, b);
    } else {
      target.insert(cand);
      target.insert(-cand);
      res_score = vfn(r, y, b);
      target.erase(cand);
      target.erase(-cand);
    }
    if (!has_current_max || current_max_score < res_score) {
      has_current_max = true;
      current_max_score = res_score;
      current_max_cand = cand;
    }
  }
  assert(has_current_max);
  insert_into->insert(current_max_cand);
  insert_into->insert(-current_max_cand);
  return optional(current_max_cand);
}

template <typename RandomDevice>
optional<Res> do_generate(int n, RandomDevice &rd) {
  Subset r(n), y(n), b(n);
  const Subset X = Subset::X(n);
  while (true) {
    Subset r_y = r + y;
    Subset x_b = Subset::X(n);
    x_b.setdiff_inplace(b);
    if (r_y == x_b) {
      break;
    }
    Subset r_cand = X.setdiff(r.setunion(y + b));
    assert(r_cand.size() % 2 == 0);
    optional<int> tm_res = try_maximize(
        r, y, b,
        [](const Subset &r, const Subset &y, const Subset &b) {
          return 2 * (r + r).size() - (r + y).size() - (r + b).size();
        },
        &r, r_cand);
    if (!tm_res.has_value()) {
      return optional<Res>();
    }
    Subset y_cand_s = X.setdiff(y.setunion(r + b));
    vector<int> y_cand(y_cand_s.begin(), y_cand_s.end());
    random_shuffle(y_cand.begin(), y_cand.end(),
                   [&](size_t n) { return rd() % n; });
    assert(y_cand.size() % 2 == 0);
    if (!try_maximize(
             r, y, b,
             [](const Subset &r, const Subset &y, const Subset &b) {
               return 2 * (y + y).size() - (y + r).size() - (y + b).size();
             },
             &y, y_cand)
             .has_value()) {
      return optional<Res>();
    }
    Subset b_cand_s = X.setdiff(b.setunion(r + y));
    b_cand_s.erase(*tm_res);
    b_cand_s.erase(-*tm_res);
    vector<int> b_cand(b_cand_s.begin(), b_cand_s.end());
    random_shuffle(b_cand.begin(), b_cand.end(),
                   [&](size_t n) { return rd() % n; });
    assert(b_cand.size() % 2 == 0);
    if (!try_maximize(
             r, y, b,
             [](const Subset &r, const Subset &y, const Subset &b) {
               return 2 * (b + b).size() - (b + r).size() - (b + y).size();
             },
             &b, b_cand)
             .has_value()) {
      return optional<Res>();
    }
    // cout << Res{.r = r, .y = y, .b = b} << endl;
  }
  if (!check_cond(r, y, b)) {
    throw runtime_error("unreachable");
  }
  return Res{.r = r, .y = y, .b = b};
}

int main(int argc, char const *argv[]) {
  Z2n1_test();
  Subset_test();
  default_random_engine dre((random_device())());
  for (int n = 3; n < 300; n++) {
    cout << "\x1b[2K\r" << n;
    cout.flush();
    for (int iter = 0; iter < 1000; iter++) {
      optional<Res> res = do_generate(n, dre);
      if (res.has_value()) {
        cout << "\x1b[2K\rFound n = " << n << '\n';
        cout << *res << '\n';
        cout.flush();
        return 0;
      }
    }
  }
  return 0;
}
