#include <bits/stdc++.h>
#include "../common.h"

using namespace std;

struct gcontext {
  mutex result_lock;
  optional<Graph> result;
  condition_variable notify_found;
  atomic_size_t nb_checked{};

  gcontext() = default;
  gcontext(const gcontext& copy) = delete;
  void found(Graph g) {
    {
      auto lg = lock_guard(result_lock);
      result = make_optional(move(g));
    }
    notify_found.notify_all();
  }
};

void do_random_checks(gcontext& gctx) {
  default_random_engine dre((random_device())());
  uniform_int_distribution<int> ndist(50, 100);
  while (true) {
    int n = ndist(dre);
    Graph g = Graph::gen_random(n, dre);
    g.discard_3color_triangles();
    if (g.check_all_triangle_extensions_fulfilled()) {
      gctx.found(move(g));
      return;
    }
    gctx.nb_checked++;
  }
}

void reporter_thread(gcontext& gctx) {
  while (true) {
    {
      auto lg = lock_guard(gctx.result_lock);
      if (gctx.result.has_value()) {
        cout << "\x1b[2K\rFound graph: \n" << gctx.result.value() << endl;
        return;
      }
    }
    size_t nb_done = gctx.nb_checked.load();
    cout << "\x1b[2K\rChecked " << nb_done << " graphs...";
    cout.flush();

    using chrono::operator""ms;
    this_thread::sleep_for(100ms);
  }
}

int main() {
  uint nb_threads = thread::hardware_concurrency();
  if (nb_threads == 0) {
    nb_threads = 4;
    cerr << "Warn: using 4 cores (can't detect)" << endl;
  }
  gcontext gctx;
  vector<thread> threads;
  threads.reserve(nb_threads);
  for (uint i = 0; i < nb_threads - 2; i ++) {
    threads.emplace_back(do_random_checks, reference_wrapper(gctx));
  }
  reporter_thread(gctx);
  // threads gets dropped here, so will terminate all other threads.
  return 0;
}
