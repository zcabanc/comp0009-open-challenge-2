#include <iostream>
#include <z3++.h>
#include <functional>

using namespace std;
using namespace z3;

/*
Problem:

Here is a problem for those would like to stretch their programming-maths abilities.  I am posing this problem for all  2nd and 3rd year undergraduates.  It is not particularly related to comp0009.  Problem (2) below is unsolved, but would be helpful to some current research.

Consider an undirected irreflexive graph G with n nodes {0, 1, …, n-1}, satisfying the following property.
For all  x\neq y<n, the pair (x, y) extends to all four possible triangle extensions in G, i.e.  for all distinct pairs x, y<n  there are four nodes z_1, z_2, z_3, z_4<n such that
(a) (x, z_1), (z_1, y) are both edges
(b) (x, z_2) is an edge but (z_2, y) is not an edge
(c) (x, z_3) is not an edge but (z_3, y) is an edge, and
(c) neither (x, z_4) nor (z_4, y) are edges.
*/

/**
 * Solve the problem assuming a given graph size n
 * @param n graph size
 * @param z3ctx z3 context
 *
 * Even though z3 supports some form of first order logic, it frequently hangs when given complicated forall/exists formula.
 * Hence we are manually expanding everything to propositional logic.
 */
bool solve_for_n(int n) {
  context z3ctx;
  solver sol(z3ctx);

  // For each pair of nodes, either they are connected or not.
  // Because the graph is symmetric and irreflexive, we only need to have variable for edge (x, y) where x < y.
  vector<vector<expr>> edge_vars(n - 1);
  for (int x = 0; x <= n - 2; x++) {
    for (int y = x + 1; y <= n - 1; y++) {
      stringstream name;
      name << "E!" << x << "!" << y;
      edge_vars[x].push_back(z3ctx.constant(name.str().c_str(), z3ctx.bool_sort()));
    }
  }
  expr _ff = z3ctx.bool_val(false);

  const auto E = [&](int x, int y) -> const expr & {
    if (x == y) {
      return _ff;
    } else if (x < y) {
      return edge_vars.at(x).at(y - x - 1);
    } else {
      return edge_vars.at(y).at(x - y - 1);
    }
  };

  // For each distinct (x, y) pair...
  for (int x = 0; x <= n - 2; x++) {
    for (int y = x + 1; y <= n - 1; y++) {
      // exists z can be written as P(0) or P(1) or P(2) or ...
      // We get rid of the possibility for z to be equal to either of x or y here, by not including P(x) and P(y).
      const auto ExistsZ = [&](const std::function<expr(int)>& predicate) -> expr {
        expr_vector ors(z3ctx);
        for (int z = 0; z < n; z++) {
          if (z == x || z == y) {
            continue;
          }
          ors.push_back(predicate(z));
        }
        return mk_or(ors);
      };

      // exists z such that E!x!z and E!z!y
      sol.add(ExistsZ([&](int z) {
        return E(x, z) && E(z, y);
      }));

      // exists z such that E!x!z and not E!z!y
      sol.add(ExistsZ([&](int z) {
        return E(x, z) && (!E(z, y));
      }));

      // exists z such that not E!x!z and E!z!y
      sol.add(ExistsZ([&](int z) {
        return (!E(x, z)) && E(z, y);
      }));

      // exists z such that not E!x!z and not E!z!y
      sol.add(ExistsZ([&](int z) {
        return (!E(x, z)) && (!E(z, y));
      }));
    }
  }

  check_result cr = sol.check();
  switch (cr) {
    case unsat:
      cout << "(n = " << n << "): unsat" << endl;
      return false;
    case sat: {
      cout << "(n = " << n << "): sat:\n";
//      auto md = sol.get_model();
//      for (int x = 0; x <= n - 2; x++) {
//        for (int y = x + 1; y <= n - 1; y++) {
//          expr has_edge = md.eval(E(x, y));
//          if (has_edge.is_true()) {
//            cout << x << ' ' << y << '\n';
//          }
//        }
//      }
      return true;
    }
    case unknown:
      throw runtime_error("z3 returned unknown (not possible for propositional logic)");
  }
}

int main(int argc, const char *const *argv) {
  for (int n = 3; n < 100; n++) {
    solve_for_n(n);
  }
}
