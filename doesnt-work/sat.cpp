#include <iostream>
#include <set>
#include <vector>
#include <functional>
#include <z3++.h>

using namespace std;
using namespace z3;

/*
 * Problem:
 *
 * Find a irreflexive, symmetric graph of n nodes such that the edges are coloured as any of {WHITE, RED, GREEN, BLACK}, and also
 * satisfies the following:
 *
 * for all pair (x, y) such that x != y,
 *    Exy = WHITE => for each of the 9 colour pairs, exists z such that Exz = pair[0] and Ezy = pair[1]
 *    Exy = RED => for each of the 7 colour pairs {(RED, RED), (RED, GREEN), (RED, BLACK), (GREEN, RED), (GREEN, GREEN), (BLACK, RED), (BLACK, BLACK)},
 *      exists z such that Exz = pair[0] and Ezy = pair[1].
 *    Exy = GREEN => ...
 *    Exy = BLUE => ...
 *
 * This solution attempts to build a propositional theorem that denotes a valid such graph with a fixed n. Then we can simply try every n.
 *
 * In this logic, ExyW denotes the edge (x, y) being white, and similar for ExyR, ExyG, ExyB.
 */

enum Color {
  WHITE = 0, RED = 1, GREEN = 2, BLACK = 3
};

bool solve_for_n(int n) {
  context z3ctx;
  solver sol(z3ctx);

  struct Eelem {
    expr white;
    expr red;
    expr green;
    expr black;
  };

  Eelem _ff_elem{
          .white = z3ctx.bool_val(true),
          .red = z3ctx.bool_val(false),
          .green = z3ctx.bool_val(false),
          .black = z3ctx.bool_val(false),
  };

  // For each pair x,y, Exy = Eyz, so we only need variables for Exy.
  // Because the graph is symmetric and irreflexive, we can assume x < y.
  vector<vector<Eelem>> edge_vars(n - 1);
  for (int x = 0; x <= n - 2; x++) {
    for (int y = x + 1; y <= n - 1; y++) {
      auto mk_expr = [&](Color c) -> expr {
        stringstream name;
        name << "E!" << x << "!" << y << "!";
        switch (c) {
          case WHITE:
            name << "W";
            break;
          case RED:
            name << "R";
            break;
          case GREEN:
            name << "G";
            break;
          case BLACK:
            name << "B";
            break;
        }
        return z3ctx.constant(name.str().c_str(), z3ctx.bool_sort());
      };
      edge_vars[x].push_back(Eelem{
              .white = mk_expr(WHITE),
              .red = mk_expr(RED),
              .green = mk_expr(GREEN),
              .black = mk_expr(BLACK),
      });
    }
  }

  const auto Egetelem = [&](int x, int y) -> const Eelem & {
    if (x == y) {
      return _ff_elem;
    } else if (x < y) {
      return edge_vars[x][y - x - 1];
    } else {
      return edge_vars[y][x - y - 1];
    }
  };

  // Lookup ExyC, returns a bool expr
  const auto E = [&](int x, int y, Color c) -> const expr & {
    const Eelem &eelem = Egetelem(x, y);
    switch (c) {
      case WHITE:
        return eelem.white;
      case RED:
        return eelem.red;
      case GREEN:
        return eelem.green;
      case BLACK:
        return eelem.black;
    }
  };

  // Now, we say that each edge must only have one colour by asserting, for all x, y:
  // ExyW and not anything else, or ExyR and not anything else, or ... or ....
  for (int x = 0; x <= n - 2; x++) {
    for (int y = x + 1; y <= n - 1; y++) {
      expr_vector ors(z3ctx);
      ors.push_back((E(x, y, WHITE)) && (!E(x, y, RED)) && (!E(x, y, GREEN)) && (!E(x, y, BLACK)));
      ors.push_back((!E(x, y, WHITE)) && (E(x, y, RED)) && (!E(x, y, GREEN)) && (!E(x, y, BLACK)));
      ors.push_back((!E(x, y, WHITE)) && (!E(x, y, RED)) && (E(x, y, GREEN)) && (!E(x, y, BLACK)));
      ors.push_back((!E(x, y, WHITE)) && (!E(x, y, RED)) && (!E(x, y, GREEN)) && (E(x, y, BLACK)));
      sol.add(mk_or(ors));
    }
  }

  // For each distinct (x, y) pair...
  // Since x,y valid <=> y,x valid (all the colour pairs are symmetric), only considering x<y.
  for (int x = 0; x <= n - 2; x++) {
    for (int y = x + 1; y <= n - 1; y++) {
      // exists z can be written as P(0) or P(1) or P(2) or ...
      // We get rid of the possibility for z to be equal to either of x or y here, by not including P(x) and P(y).
      const auto ExistsZ = [&](const std::function<expr(int)> &predicate) -> expr {
        expr_vector ors(z3ctx);
        for (int z = 0; z < n; z++) {
          if (z == x || z == y) {
            continue;
          }
          ors.push_back(predicate(z));
        }
        return mk_or(ors);
      };

      // There are 4 cases, hence the assertion is a "and" of 4 "=>"s. We can add the causes individually.
      for (Color c : std::array<Color, 4>{WHITE, RED, GREEN, BLACK}) {
        set<pair<Color, Color>> expect_pairs{
                {RED,   RED},
                {RED,   GREEN},
                {RED,   BLACK},
                {GREEN, RED},
                {GREEN, GREEN},
                {GREEN, BLACK},
                {BLACK, RED},
                {BLACK, GREEN},
                {BLACK, BLACK},
        };

        switch (c) {
          case WHITE:
            // We expect all pairs
            assert(expect_pairs.size() == 9);
            break;
          case RED:
            expect_pairs.erase(make_pair(GREEN, BLACK));
            expect_pairs.erase(make_pair(BLACK, GREEN));
            assert(expect_pairs.size() == 7);
            break;
          case GREEN:
            expect_pairs.erase(make_pair(RED, BLACK));
            expect_pairs.erase(make_pair(BLACK, RED));
            assert(expect_pairs.size() == 7);
            break;
          case BLACK:
            expect_pairs.erase(make_pair(RED, GREEN));
            expect_pairs.erase(make_pair(GREEN, RED));
            assert(expect_pairs.size() == 7);
            break;
        }

        // For all colour pairs in expect_pairs...
        expr_vector ands(z3ctx);
        for (const pair<Color, Color> &p : expect_pairs) {
          ands.push_back(ExistsZ([&](int z) {
            return E(x, z, p.first) && E(z, y, p.second);
          }));
        }

        sol.add(implies(E(x, y, c), mk_and(ands)));
      }
    }
  }

  // Finally, we also need to make sure no forbidden triangles.
  // In this case we need to consider all possible x,y,z pairs, but if we do that then we only
  // need to test one colour combination - ExyR and EyzG and EzxB, since by swapping some of x,y,z we
  // can get any combination.

//  for (int x = 0; x < n; x ++) {
//    for (int y = 0; y < n; y ++) {
//      for (int z = 0; z < n; z ++) {
//        sol.add(!(E(x,y,RED) && E(y,z,GREEN) && E(z,x,BLACK)));
//      }
//    }
//  }

  cout << "[PERF]: Built theorem" << endl;

  check_result cr = sol.check();
  switch (cr) {
    case unsat:
      cout << "(n = " << n << "): unsat" << endl;
      return false;
    case sat: {
      cout << "(n = " << n << "): sat:\n";
      model mdl = sol.get_model();
      for (int x = 0; x <= n - 2; x++) {
        for (int y = x + 1; y <= n - 1; y++) {
          cout << x << ' ' << y << ' ';
          const Eelem &eelem = Egetelem(x, y);
          if (mdl.eval(eelem.white).is_true()) {
            cout << 'W';
          } else if (mdl.eval(eelem.red).is_true()) {
            cout << 'R';
          } else if (mdl.eval(eelem.green).is_true()) {
            cout << 'G';
          } else if (mdl.eval(eelem.black).is_true()) {
            cout << 'B';
          } else {
            throw runtime_error("unreachable");
          }
          cout << '\n';
        }
      }
      cout.flush();
      return true;
    }
    case unknown:
      throw runtime_error("Not supposed to happen for propositional logic.");
  }

  return false;
}

int main(int argc, const char *const *argv) {
  solve_for_n(72);
}
