#include <bits/stdc++.h>
#include <z3++.h>
#include "../common.h"

using namespace std;
using namespace z3;

bool do_graph_iter(Graph &g) {
  if (g.n <= 3) {
    return false;
  }

  context z3ctx;
  solver sol(z3ctx);
  int n = g.n;

  vector<vector<expr>> retain_edge_vars(n);
  for (int x = 0; x <= n - 2; x++) {
    for (int y = x + 1; y <= n - 1; y++) {
      // We always start with a complete graph to maximize the chances.
      assert(g.get_edge_color(x, y) != NONE);
      stringstream var_name;
      var_name << "R!" << x << '!' << y;
      retain_edge_vars.at(x).push_back(z3ctx.bool_const(var_name.str().c_str()));
    }
  }

  auto Retain = [&](int x, int y) -> const expr & {
    assert(x >= 0 && x < g.n && y >= 0 && y < g.n);
    if (x > y) {
      swap(x, y);
    }
    if (x == y) {
#ifdef NDEBUG
      __builtin_unreachable();
#endif
      throw invalid_argument("x == y");
    } else /* if (x < y) */ {
      int y_idx = y - x - 1;
      return retain_edge_vars[x][y_idx];
    }
  };

  // Forbid 3 colour triangles
  set<std::array<int, 3>> done_triangles;
  auto process_triangle = [&](int a, int b, int c) {
    assert(g.get_edge_color(a, b) != g.get_edge_color(a, c) && g.get_edge_color(a, b) != g.get_edge_color(b, c) &&
           g.get_edge_color(a, c) != g.get_edge_color(b, c));
    assert(g.get_edge_color(a, b) != NONE && g.get_edge_color(a, c) != NONE && g.get_edge_color(b, c) != NONE);
    std::array<int, 3> tri_points{a, b, c};
    std::sort(tri_points.begin(), tri_points.end());
    if (done_triangles.find(tri_points) != done_triangles.end()) {
      return;
    }
    done_triangles.insert(tri_points);

    // need to remove at least one edge to break the triangle, hence NOT (ab && ac && bc)
    sol.add(!(Retain(a, b) && Retain(b, c) && Retain(a, c)));
  };

  for (const auto&[connected_pair, color_ab] : g.edges) {
    int a = connected_pair.first;
    int b = connected_pair.second;
    assert(color_ab != NONE);
    /*
     *    a---------b
     *      .      .
     *        .  .
     *          c
     */
    for (auto[c, color_bc] : g.neighbours(b)) {
      assert(color_bc != NONE);
      if (c == a || color_bc == color_ab) {
        continue;
      }
      auto color_ac = g.get_edge_color(a, c);
      if (color_ac != NONE && color_ac != color_ab && color_ac != color_bc) {
        process_triangle(a, b, c);
      }
    }
  }

  for (int x = 0; x <= n - 2; x++) {
    for (int y = x + 1; y <= n - 1; y++) {
      map<pair<Color, Color>, expr_vector> cond_for_color_pair;
      std::array<Color, 3> colors{RED, GREEN, BLACK};
      Color xy_color = g.get_edge_color(x, y);

      for (Color color1 : colors) {
        for (Color color2 : colors) {
          auto res = cond_for_color_pair.emplace(make_pair(color1, color2), expr_vector(z3ctx));
          assert(res.second);
        }
      }

      for (auto[z, xz_color] : g.neighbours(x)) {
        if (z == y) {
          continue;
        }
        Color yz_color = g.get_edge_color(y, z);
        if (yz_color == NONE) {
          continue;
        }
        pair<Color, Color> cp{xz_color, yz_color};
        expr_vector &cond_vec = cond_for_color_pair.find(cp)->second;
        cond_vec.push_back(Retain(x, z) && Retain(y, z));
      }

      for (Color color1 : colors) {
        for (Color color2 : colors) {
          pair<Color, Color> cp{color1, color2};
          auto iter = cond_for_color_pair.find(cp);
          assert(iter != cond_for_color_pair.end());
          expr_vector cond_vec = iter->second; // no copy here, expr_vector is reference counted.
#ifndef NDEBUG
          cond_for_color_pair.erase(iter);
#endif
          if (xy_color == NONE || !(color1 != xy_color && color2 != xy_color && color1 != color2)) {
            if (cond_vec.empty()) {
              return false;
            }
            sol.add(mk_or(cond_vec));
          } else {
            if (cond_vec.empty()) {
              sol.add(Retain(x, y));
            } else {
              sol.add(implies(!Retain(x, y), mk_or(cond_vec)));
            }
          }
        }
      }
    }
  }

  cout << " -- Solving...";
  cout.flush();

  check_result cr = sol.check();
  switch (cr) {
    case unsat:
      return false;
    case sat: {
      model mdl = sol.get_model();
      for (int x = 0; x <= n - 2; x++) {
        for (int y = x + 1; y <= n - 1; y++) {
          if (mdl.eval(Retain(x, y)).is_false()) {
            g.set_edge(x, y, NONE);
          }
        }
      }
      return true;
    }
    case unknown:
      throw runtime_error("Not possible for propositional logic.");
  }
}

int main(int argc, const char *const *argv) {
  if (argc != 3) {
    cerr << "Usage: random-sat <lower> <upper>" << endl;
    return 1;
  }
  int lower = stoi(argv[1]);
  int upper = stoi(argv[2]);
  default_random_engine dre((random_device()) ());
  uniform_int_distribution<int> ndist(lower, upper);
  size_t nb_graph_checked = 0;
  size_t nb_solved = 0;
  using std::chrono::operator""ms;
  using std::chrono::steady_clock;
  auto last_msg = steady_clock::now();
  auto update_status = [&](int n) {
    cout << "\x1b[2K\rchecking n = " << n << " (checked " << nb_graph_checked << " graphs, for which " << nb_solved
         << " were handed to SAT.)";
    cout.flush();
  };
  while (true) {
    Graph rg = Graph::gen_random(ndist(dre), dre);
    cout << rg << endl;
    if (steady_clock::now() > last_msg + 100ms) {
      update_status(rg.n);
      last_msg = steady_clock::now();
    }
    nb_graph_checked++;
    if (!rg.check_all_triangle_extensions_fulfilled()) {
      continue;
    }
    update_status(rg.n);
    nb_solved++;
    if (do_graph_iter(rg)) {
      cout << "\x1b[2K\r";
      cout << rg << endl;
      return 0;
    }
  }
}
