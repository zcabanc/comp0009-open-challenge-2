#include <iostream>
#include <utility>
#include <map>
#include <set>
#include <random>
#include <assert.h>

enum Color {
  RED = 1, GREEN = 2, BLACK = 3, NONE = 0
};

const char* color_to_text(Color c);

std::ostream& operator<<(std::ostream& os, Color c);

inline std::pair<Color, Color> two_other_colors(Color c) {
  switch (c) {
    case RED:
      return {GREEN, BLACK};
    case GREEN:
      return {RED, BLACK};
    case BLACK:
      return {RED, GREEN};
    case NONE:
      assert(false);
      __builtin_unreachable();
  }
}

class Graph {
public:
  int n;
  std::map<std::pair<int, int>, Color> edges;

  explicit Graph(int n) : n(n) {
    assert(n >= 0);
  }

  void set_edge(int a, int b, Color c) {
    using std::make_pair;

    assert(a < n && b < n && a >= 0 && b >= 0);
    assert(a != b);
    if (c != NONE) {
      edges.insert_or_assign(make_pair(a, b), c);
      edges.insert_or_assign(make_pair(b, a), c);
    } else {
      edges.erase(make_pair(a, b));
      edges.erase(make_pair(b, a));
    }
  }

  class NeighbourIterator {
    int from_a;
    using inner_it_t = std::map<std::pair<int, int>, Color>::const_iterator;

    using value_type = std::pair<int, Color>;
    using iterator_category = std::input_iterator_tag;

    inner_it_t inner_it;
    inner_it_t inner_it_end;
    bool ended;

  public:
    NeighbourIterator() {
      ended = true;
    };

    NeighbourIterator(const Graph &g, int a) : from_a(a), inner_it(g.edges.lower_bound(std::make_pair(a, 0))),
                                               inner_it_end(g.edges.end()), ended(false) {
      if (inner_it == inner_it_end || inner_it->first.first != a) {
        ended = true;
      }
    }

    NeighbourIterator operator++() {
      ++inner_it;
      if (inner_it == inner_it_end || inner_it->first.first != from_a) {
        ended = true;
      }
      return *this;
    }

    value_type operator*() const {
      auto val = *inner_it;
      return std::make_pair(val.first.second, val.second);
    }

    friend bool operator==(const NeighbourIterator &a, const NeighbourIterator &b) {
      if (a.ended && b.ended) {
        return true;
      } else if (a.ended ^ b.ended) {
        return false;
      } else {
        return a.inner_it == b.inner_it;
      }
    }

    friend bool operator!=(const NeighbourIterator &a, const NeighbourIterator &b) {
      return !(a == b);
    }
  };

  class Neighbours {
    const Graph &g;
    int a;
  public:
    Neighbours(const Graph &g, int a) : g(g), a(a) {}

    NeighbourIterator begin() const {
      return NeighbourIterator(g, a);
    }

    NeighbourIterator end() const {
      return NeighbourIterator();
    }
  };

  [[nodiscard]] Neighbours neighbours(int a) const {
    return Neighbours(*this, a);
  }

  static std::pair<int, int> ord_pair(std::pair<int, int> p) {
    return ord_pair(p.first, p.second);
  }

  static std::pair<int, int> ord_pair(int a, int b) {
    using std::make_pair;

    if (a <= b) {
      return make_pair(a, b);
    } else {
      return make_pair(b, a);
    }
  }

  void discard_3color_triangles();

  template<typename Generator>
  static Graph gen_random(int n, Generator& rng) {
    using std::uniform_int_distribution;

    uniform_int_distribution<int> intdist(RED, BLACK);
    Graph g(n);
    for (int i = 0; i <= n - 2; i++) {
      for (int j = i + 1; j <= n - 1; j++) {
        g.set_edge(i, j, (Color) intdist(rng));
      }
    }
    return g;
  }

  friend std::ostream& operator<<(std::ostream& os, const Graph& g) {
    os << "n = " << g.n << '\n';
    for (auto [pair, color] : g.edges) {
      os << pair.first << " " << pair.second << " " << color << '\n';
    }
    return os;
  }

  Color get_edge_color(int x, int y) const {
    Color cl = NONE;
    auto f = edges.find(std::make_pair(x, y));
    if (f != edges.cend()) {
      cl = f->second;
    }
    return cl;
  }

  [[nodiscard]] bool check_pair_satisfies_condition(int x, int y) const;

  [[nodiscard]] bool check_all_triangle_extensions_fulfilled() const;
};
