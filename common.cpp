#include <bits/stdc++.h>
#include "common.h"

using namespace std;

const char* color_to_text(Color c) {
  switch (c) {
    case RED:
      return "red";
    case GREEN:
      return "green";
    case BLACK:
      return "black";
    case NONE:
      return "none";
  }
}

ostream& operator<<(ostream& os, Color c) {
  return os << color_to_text(c);
}

bool Graph::check_pair_satisfies_condition(int x, int y) const {
  using std::set;
  using std::pair;
  using std::make_pair;

  assert(x < y);
  assert(x < n && x >= 0);
  assert(y < n && y >= 0);

  set<pair<Color, Color>> seen_combinations;
  Color xy_color = get_edge_color(x, y);
  if (xy_color != NONE) {
    auto other_colors = two_other_colors(xy_color);
    seen_combinations.emplace(other_colors.first, other_colors.second);
    seen_combinations.emplace(other_colors.second, other_colors.first);
  }
  for (auto [z, xz_color] : neighbours(x)) {
    if (z == y) {
      continue;
    }
    Color yz_color = get_edge_color(y, z);
    if (yz_color == NONE) {
      continue;
    }
    seen_combinations.insert(make_pair(xz_color, yz_color));
  }
  return seen_combinations.size() == 9;
}

bool Graph::check_all_triangle_extensions_fulfilled() const {
  if (n < 3) {
    return false;
  }
  for (int i = 0; i <= n - 2; i ++) {
    for (int j = i + 1; j <= n - 1; j ++) {
      if (!check_pair_satisfies_condition(i, j)) {
        return false;
      }
    }
  }
  return true;
}

void Graph::discard_3color_triangles() {
  std::set<std::pair<int, int>> edges_to_remove;
  for (const auto&[connected_pair, color_ab] : edges) {
    int a = connected_pair.first;
    int b = connected_pair.second;
    assert(color_ab != NONE);
    /*
     *    a---------b
     *      .      .
     *        .  .
     *          c
     */
    if (edges_to_remove.find(ord_pair(a, b)) != edges_to_remove.end()) {
      continue;
    }
    for (auto[c, color_bc] : this->neighbours(b)) {
      if (c == a) {
        continue;
      }
      if (color_bc != color_ab) {
        if (edges_to_remove.find(ord_pair(b, c)) != edges_to_remove.end()) {
          continue;
        }
        if (edges_to_remove.find(ord_pair(a, c)) != edges_to_remove.end()) {
          continue;
        }
        auto ac_edge = edges.find(std::make_pair(a, c));
        if (ac_edge != edges.end()) {
          Color color_ac = ac_edge->second;
          if (color_ac != color_ab && color_ac != color_bc) {
            edges_to_remove.insert(ord_pair(connected_pair));
            break;
          }
        }
      }
    }
  }
  for (auto p : edges_to_remove) {
    this->set_edge(p.first, p.second, NONE);
  }
}
