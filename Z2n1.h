#pragma once
#include <algorithm>
#include <assert.h>
#include <iostream>
#include <unordered_set>
#include <vector>

struct Z2n1 {
  int n;
  int val;

  Z2n1(int n, int val) : n(n), val(val) { assert(n > 0); }
  Z2n1 operator+(const Z2n1 &other) const {
    assert(this->n == other.n);
    int nval = this->val + other.val;
    return Z2n1(n, (nval + n + (2*n+1)) % (2 * n + 1) - n);
  }
  bool operator==(const Z2n1 &other) const {
    return n == other.n && val == other.val;
  }

  operator int() const { return val; }
};

struct Subset {
  std::unordered_set<int> content;
  int n;

  Subset(int n) : n(n) { assert(n > 0); }
  Subset(int n, std::unordered_set<int> content)
      : n(n), content(move(content)) {
    assert(n > 0);
  }
  decltype(content.begin()) begin() { return content.begin(); }
  decltype(content.cbegin()) begin() const { return content.cbegin(); }
  decltype(content.end()) end() { return content.end(); }
  decltype(content.cend()) end() const { return content.cend(); }
  void insert(int val) {
    assert(val >= -n && val <= n);
    this->content.insert(val);
  }
  void insert(Z2n1 val) {
    assert(val.n == this->n);
    this->content.insert(val.val);
  }
  Subset operator+(const Subset &other) const {
    Subset res(n);
    for (int x : other) {
      for (int y : this->content) {
        res.insert(Z2n1(n, x) + Z2n1(n, y));
      }
    }
    return res;
  }
  bool operator==(const Subset &other) const {
    return other.n == this->n && other.content == this->content;
  }
  bool operator!=(const Subset &other) const { return !(*this == other); }
  void setdiff_inplace(const Subset &other) {
    for (int v : other) {
      this->content.erase(v);
    }
  }
  Subset setdiff(const Subset &other) const {
    Subset cloned = *this;
    cloned.setdiff_inplace(other);
    return cloned;
  }
  void setunion_inplace(const Subset &other) {
    for (int v : other) {
      this->content.insert(v);
    }
  }
  Subset setunion(const Subset &other) const {
    Subset cloned = *this;
    cloned.setunion_inplace(other);
    return cloned;
  }
  bool contains(int v) const { return content.find(v) != content.end(); }
  bool erase(int v) { return this->content.erase(v); }
  int size() const { return content.size(); }

  static Subset full(int n) {
    Subset s(n);
    for (int i = -n; i <= n; i++) {
      s.content.insert(i);
    }
    return s;
  }

  static Subset X(int n) {
    Subset s = Subset::full(n);
    s.content.erase(0);
    return s;
  }
  friend std::ostream &operator<<(std::ostream &os, const Subset &s) {
    os << '{';
    bool first = true;
    std::vector<int> s_sorted(s.content.begin(), s.content.end());
    std::sort(s_sorted.begin(), s_sorted.end());
    for (int v : s_sorted) {
      if (!first) {
        os << ", ";
      }
      first = false;
      os << v;
    }
    os << '}';
    return os;
  }
};

void Z2n1_test() {
  assert(Z2n1(3, 1) + Z2n1(3, 2) == Z2n1(3, 3));
  assert(Z2n1(3, 1) + Z2n1(3, 3) == Z2n1(3, -3));
  assert(Z2n1(3, 2) + Z2n1(3, 4) == Z2n1(3, -1));
}

void Subset_test() {
  using namespace std;
  assert(Subset(3, unordered_set<int>{1, 2}) +
             Subset(3, unordered_set<int>{0, -2}) ==
         Subset(3, unordered_set<int>{1, 2, -1, 0}));
  assert(Subset(3, unordered_set<int>{-3, 3}) +
             Subset(3, unordered_set<int>{-3, 3}) ==
         Subset(3, unordered_set<int>{-1, 0, 1}));
  assert(Subset(7, unordered_set<int>{-3, 3}) +
             Subset(7, unordered_set<int>{-3, 3}) ==
         Subset(7, unordered_set<int>{-6, 0, 6}));
}
